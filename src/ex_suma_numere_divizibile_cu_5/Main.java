package ex_suma_numere_divizibile_cu_5;

public class Main {
    public static void main(String[] args) {
       // sa se calculeze suma numerelor divizibile cu 5 din intervalul 0 - 50
        int sum = 0;

        for (int i = 0; i <= 50; i++){
            if (i %5 == 0) {
                sum = sum + i; // sum +=i;

                System.out.println(i);
            }
        }
        System.out.println("suma este: " + sum);
    }
}
