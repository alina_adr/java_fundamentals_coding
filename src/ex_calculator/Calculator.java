package ex_calculator;

public class Calculator {
    // acesta este un constructor (default)
    public Calculator(){

    }
    public int suma(int a, int b) {
        return a + b;
    }

    public int scadere(int a, int b) {
        return a - b;
    }

    public int inmultire(int a, int b) {
        return a * b;
    }

    public double impartire(int a, int b) {
        return (double) a / b;
    }
}
