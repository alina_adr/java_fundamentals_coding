package ex_calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        readData();

    }

    public static void readData() {
// sa se creeze un calculator care sa efectueze operatii de adunare, scadere, inmultire si impartire pentru 2 numere a si b
        Calculator calc = new Calculator();

        Scanner scanner = new Scanner(System.in);

        System.out.println("introduceti a: ");
        int a = scanner.nextInt();

        System.out.println("introduceti b: ");
        int b = scanner.nextInt();

        Scanner commandScanner = new Scanner(System.in);

        // aceasta este o bucla infinita
        // do {
        //} while (true);

        String command;
        do {


            System.out.println("alege o comanda de mai jos");
            System.out.println("+ adunare");
            System.out.println("- scadere");
            System.out.println("* inmultire");
            System.out.println("/ impartire");
            System.out.println("pentru oprire scriem stop");

            // metoda next() citeste un cuvant de la tastatura
            // metoda nextLine() citeste textul pana intalneste enter
            command = commandScanner.next();
            switch (command) {
                case "+":
                    int resultSuma = calc.suma(a, b);
                    System.out.println(resultSuma);
                    break;

                case "-":
                    int resultScadere = calc.scadere(a, b);
                    System.out.println(resultScadere);
                    break;

                case "*":
                    int resultInmultire = calc.inmultire(a, b);
                    System.out.println(resultInmultire);
                    break;

                case "/":
                    double resultImpartire = calc.impartire(a, b);
                    System.out.println(resultImpartire);
                    break;

                default:
                    System.out.println("comanda introdusa nu exista");
            }

        } while (!command.equals("stop"));
    }
}
