public class Main {
    public static void main(String[] args) {
        System.out.println("Hello Alina!");
    }
}

// git init este comanda de initializare a unui proiect de git local
// git add . este comanda de adaugare a fisierelor noi / modificarilor noi in git local
// git commit -m "mesajul meu de commit" este comanda care salveaza modificarile in git local (mesajul unui commit trebuie sa descrie modificarile din commit)
// git remote add origin este comanda care face conexiunea dintre proiectul remote (gitlab) si proiectul local (de pe calculator)
// git push --set-upstream origin master este comanda care trimite catre remote (gitlab) ramura principala (master) a proiectului
// in git avem notiunea de branch (ramura) atunci cand avem un branch nou la primul push trebuie sa adaugam --set-upstream origin [nume branch], acceasta comanda se executa doar odata, apoi se poate executa doar git push

// git checkout -b [nume branch] este comanda care creaza o noua ramura (branch)
// IMPORTANT pentru a putea schimba ramura (branch) inapoi pe master, toate modificarile aduse trebuie sa fie salvate intr-un commit
// git checkout [nume branch] este comanda care ne ajuta sa trecem de pe o ramura pe alta

// git pull este comanda care descarca modificarile din remote (gitlab)
