package ex_manipulare_stringuri;

public class Main {
    public static void main(String[] args) {
         afisareCaractereDinString();

        calculareNumarCuvinteDinString();

    }

    public static void afisareCaractereDinString() {
        // sa se afiseze toate caracterele unui sting in consola
        // string: Ana are mere

        String text = "Ana are mere";
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            System.out.println(c);
        }
    }

    public static void calculareNumarCuvinteDinString() {

        String text = "Ana are mere";
        String[] words = text.split(" ");
        System.out.println(words.length);
    }
}
