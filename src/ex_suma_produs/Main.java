package ex_suma_produs;

public class Main {
    public static void main(String[] args) {
        // Sa se calculeze suma primelor 5 numere divizibile cu 3 si produsul primelor 4 numere divizibile cu 2
        int suma = 0;
        int nrDivizibileCu3 = 5;
        int i = 0;
        while (nrDivizibileCu3 > 0) {
            if (i % 3 == 0) {
                System.out.println(i);
                nrDivizibileCu3 --; // atunci cand gasim un numar divizibil cu 3 decrementam nrDivizibileCu3 (variabila)
                suma = suma + i;
            }
            i++;
        }
        System.out.println("suma primelor 5 numere divizibile cu 3 este: " + suma);
        System.out.println("valoarea lui i este; " +i);



        int produs = 1;
        int nrDivizibileCu2 = 4;
        i = 1;
        while (nrDivizibileCu2 > 0) {
            if (i % 2 == 0) {
                System.out.println(i);
                nrDivizibileCu2 --;
                produs = produs * i;
            }
            i ++;
        }
        System.out.println("produsul primelor 4 numere divizibile cu 2 este: " + produs );
        System.out.println("valoarea lui i este; " +i);




    }
}
